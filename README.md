# Deploy Alpine

Docker image for the deployment of applications on hosts accessible via ssh.

- Base Image: alpine:3
- Installed Packages: openssh-client, ca-certificates, git
